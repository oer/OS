quizJavaThreads = {
    "info": {
        "name":    "", // Should be empty with emacs-reveal
        "main":    "What does a thread do?",
        "level1":  "Excellent!",          // 80-100%
        "level2":  "Please re-try.",      // 60-79%
        "level3":  "Please re-try.",      // 40-59%
        "level4":  "Maybe ask for help?", // 20-39%
        "level5":  "Please ask for help." //  0-19%, no comma here
    },
    "questions": [
	{
            "q": "Select the correct statement about Java threads.",
            "a": [
                {"option": "Every class defining the code for a thread is a subclass of Thread.", "correct": false},
                {"option": "The code for a thread is defined in the method run().", "correct": true},
                {"option": "A thread executes its code immediately after its creation.", "correct": false},
                {"option": "A thread executes its code when run() is invoked.", "correct": false}
            ],
            "correct": "<p><span>Correct!</span></p>",
            "incorrect": "<p><span>No.</span> Please check out <a href=\"Operating-Systems-Threads.html#slide-java-threads\">earlier slides</a> to see where code is defined and what to invoke as programmer.</p>" // no comma here
        }
    ]
};
