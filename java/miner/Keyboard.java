// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: 2022 Jens Lechtenbörger

import java.util.*;

/**
 * The class Keyboard serves as example for an I/O bound thread.
 *
 * The thread repeatedly reads lines (multiple characters ended by a
 * newline) from the keyboard and stores them into a bounded buffer.
 */
 public class Keyboard implements Runnable
{
    private BoundedBuffer buffer;
    private String name;

    public Keyboard(BoundedBuffer b, String n) {
        buffer = b;
        name = n;
    }

    public void run() {
        String line;
        Scanner scanner = new Scanner(System.in);
        while (true) {
            try {
                line = scanner.nextLine();
                buffer.insert(line);
                System.out.println(name + " entered: " + line);
            } catch(InterruptedException e) { }
      }
   }
}
