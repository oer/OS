quizCryptoHashing = {
    "info": {
        "name":    "", // Should be empty with emacs-reveal
        "main":    "Cryptographic hashing is a wonder.",
        "level1":  "Excellent!",          // 80-100%
        "level2":  "Please re-try.",      // 60-79%
        "level3":  "Please re-try.",      // 40-59%
        "level4":  "Maybe ask for help?", // 20-39%
        "level5":  "Please ask for help." //  0-19%, no comma here
    },
    "questions": [
	{
            "q": "Select correct statements.",
            "a": [
                {"option": "Cryptographic hash functions are a subclass of hash functions that aim for collision resistance.", "correct": true},
                {"option": "Hash values can be computed by anyone (including attackers) as hash functions are publicly standardized.", "correct": true},
                {"option": "Strong collision resistance implies weak collision resistance, i.e., every hash function that is strongly collision resistant is also weakly collision resistant.", "correct": true},
                {"option": "Hash functions that are not strongly collision resistant are useless for digital signatures.", "correct": false}
            ],
            "correct": "<p><span>Correct!</span></p>",
            "incorrect": "<p><span>No. (Hint: 3 statements are correct.)</span> Maybe ask?</p>" // no comma here
        }
    ]
};
