/*
  SPDX-FileCopyrightText: 2023 Jens Lechtenbörger
  SPDX-License-Identifier: GPL-3.0-or-later

  Read any number of lines from the keyboard.
  Print the number of read lines.
*/

import java.io.*;

public class StandardInput {
    public static void main(String[] args) {
        System.out.println("Please enter some text.");
        System.out.println("(Finish with your system's end-of-file [EOF] signal, e.g., Ctrl-D, Ctrl-Z, ...)");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.printf("Got %d lines.%n", reader.lines().count());
    }
}
