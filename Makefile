# SPDX-FileCopyrightText: 2020,2022 Jens Lechtenbörger
# SPDX-License-Identifier: CC0-1.0

ORGS :=
SOCKET_ID :=

.PHONY: all build build-multiplex clean

all: clean build zip printed-pdfs

build:
	emacs --batch --load elisp/publish.el

build-multiplex: multiplex
	emacs --batch --load elisp/publish.el

clean:
	rm -f *.aux *.bbl *.fls *.html *latexmk *.tex *.tex~ *.pdf
	rm -rf public
	rm -rf ~/.org-timestamps

zip:
	apt install zip wget
	rm -rf public/eval
	rm -f os*.zip
	zip -r os.zip public -x \*.pdf \*.mp4
	find public -name "*.pdf" -print | zip os-pdf.zip -@

printed-pdfs:
	rm -rf tmp/
	rm OS-printed-pdfs.zip
	mkdir -p tmp
	wget https://gitlab.com/oer/OS/-/jobs/artifacts/master/download?job=pages -O tmp/os-artifacts.zip
	unzip -d tmp tmp/os-artifacts.zip
	cd tmp && find public/pdfs -name "*.pdf" -print | zip ../OS-printed-pdfs.zip -@

multiplex:
	$(foreach org,$(ORGS),./bin/create_multiplex.sh $(org) $(SOCKET_ID);)
