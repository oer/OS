quizCatPipe = {
    "info": {
        "name":    "", // Should be empty with emacs-reveal
        "main":    "Pipelines starting with <code>cat file | ...</code>, e.g., <code>cat vehicles | grep \"L337.*9\"</code>, are ugly and should not be used.",
        "level1":  "Excellent!",          // 80-100%
        "level2":  "Please re-try.",      // 60-79%
        "level3":  "Please re-try.",      // 40-59%
        "level4":  "Maybe ask for help?", // 20-39%
        "level5":  "Please ask for help." //  0-19%, no comma here
    },
    "questions": [
	{
            "q": "Select correct statements about cats and pipes.",
            "a": [
                {"option": "Command <code>cat</code> outputs the <code>file</code> to stdout (of the process for <code>cat</code>).", "correct": true},
                {"option": "The pipeline creates two processes.", "correct": true},
                {"option": "The pipeline creates three processes.", "correct": false},
                {"option": "With such pipelines, commands such as <code>grep</code> work (via their stdin) on contents of the input file of <code>cat</code>.", "correct": true},
                {"option": "Access of file contents can be performed with redirection of stdin (<code>&lt;</code>).", "correct": true},
                {"option": "Access of file contents on stdin does not require additional processes.", "correct": true},
                {"option": "Such pipelines exemplify an <a href=\"https://en.wikipedia.org/wiki/Anti-pattern\">anti-pattern</a>.", "correct": true}
            ],
            "correct": "<p><span>Correct!</span></p>",
            "incorrect": "<p><span>No. (Hint: 6 statements are correct.)</span> Please revisit pipelines and redirection.  Maybe ask?</p>" // no comma here
        }
    ]
};
