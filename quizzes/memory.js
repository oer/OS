quizMemory = {
    "info": {
        "name":    "", // Should be empty with emacs-reveal
        "main":    "Memorizing memory",
        "level1":  "Excellent!",          // 80-100%
        "level2":  "Please re-try.",      // 60-79%
        "level3":  "Please re-try.",      // 40-59%
        "level4":  "Maybe ask for help?", // 20-39%
        "level5":  "Please ask for help." //  0-19%, no comma here
    },
    "questions": [
	{
            "q": "Select correct statements about virtual addresses.",
            "a": [
                {"option": "Virtual addresses exhibit a hierarchical structure.", "correct": true},
                {"option": "The size of the virtual address space is defined by the CPU architecture, not the size of physical memory.", "correct": true},
                {"option": "The OS maintains a page table to keep track of what data resides where in RAM.", "correct": false},
                {"option": "The number of address bits determines the size of the virtual address space.", "correct": true}
            ],
            "correct": "<p><span>Correct!</span></p>",
            "incorrect": "<p><span>No. (Hint: 3 statements are correct. Note that the third statement uses singular.)</span> Maybe read <a href=\"./texts/virtual-addressing.pdf\">this text</a>.</p>" // no comma here
        },
        {
            "q": "Select correct statements related to paging.",
            "a": [
                {"option": "Page tables provide mappings between pages and frames.", "correct": true},
                {"option": "The MMU maintains page tables to keep track of RAM locations of pages.", "correct": false},
                {"option": "Page faults are managed by an interrupt handler.", "correct": true},
                {"option": "With paging, offsets remain unchanged.", "correct": true}
            ],
            "correct": "<p><span>Correct!</span></p>",
            "incorrect": "<p><span>No. (Hint: 3 statements are correct.)</span> Maybe read <a href=\"./texts/virtual-addressing.pdf\">this text</a>.</p>" // no comma here
        }
    ]
};
