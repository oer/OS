// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: 2022 Jens Lechtenbörger

/**
 * Main class to create Keyboard thread and Miner threads.
 *
 * As shared data structure, use a bounded buffer.  We will revisit
 * bounded buffers in detail in the context of mutual exclusion (MX).
 * For now, such details are not important, but the comments at the
 * beginning of the file BoundedBuffer.java should be sufficient to
 * understand what is going on here.
 */
public class MinerTest
{
    public static void main(String args[]) {
        if (args.length == 0) {
            System.err.println("Need an argument!");
            System.exit(1);
        }
        BoundedBuffer buffer = new SynchronizedBoundedBuffer();

        Thread keyboardInput = new Thread(new Keyboard(buffer, "Keyboard reader"));
        keyboardInput.start();

        int noMiners = Integer.parseInt(args[0]);
        Thread[] miners = new Thread[noMiners];
        for (int i = 0; i<noMiners; i++) {
            miners[i] = new Thread(new Miner(buffer, "Miner" + i));
            miners[i].start();
        }
    }
}
